/**
Pseudo code for CountDownEvent
*/

static volatile unsigned int count = 0;
const unsigned int WAITER = 0x80000000;

void inc(unsigned int num)
{
	atomic_add(&count, num);
}

void wait()
{
	if(count)
	{
		unsigned int prev = atomic_or(&count, WAITER);
		if(prev)
		{
			wait_queue.push_back(getCurrentThread());
			waitUntilSignaled();
		}
		count = 0;
	}
}

void dec()
{
	unsigned int prev = atomic_dec(&count);
	if(prev == WAITER + 1)
	{
		while(wait_queue.isEmpty());
		while(!wait_queue.isEmpty())
		{
			wakeUpThread(wait_queue.pop());
		}
	}
	
}