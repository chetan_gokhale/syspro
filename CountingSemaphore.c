#include <Windows.h>

#define RESOURCE_COUNT 5
#define SIZE	5

typedef struct __queue
{
	HANDLE Q[SIZE];
	unsigned int head;
	unsigned int tail;
}queue;

unsigned int isEmpty(queue q)
{
	if(q.head == q.tail)
		return 1;
	else
		return 0;
}

unsigned int isFull(queue q)
{
	if(q.head == (q.tail + 1)%SIZE)
		return 1;
	else
		return 0;
}

void enqueue(queue q, HANDLE tid)
{
	if(!isFull(q))
	{
		q.Q[q.tail] = tid;
		q.tail = (q.tail+1)%SIZE;
	}
}

HANDLE dequeue(queue q)
{
	HANDLE tid = 0;
	if(!isEmpty(q))
	{
		tid = q.Q[q.head];
		q.Q[q.head] = 0;
		q.head = (q.head+1)%SIZE;
	}
	return tid;
}

typedef struct __semaphore
{
	volatile unsigned int count;
	volatile queue q;
} Semaphore;

void __wait(Semaphore S)
{
	// disable interrupts
	if(S.count > 0)
	{
		S.count--;
		//enable interrupts
		return;
	}
	enqueue(S.q, GetCurrentThread());
	//enable interrupts
	SuspendThread(GetCurrentThread());
}

void __signal(Semaphore S)
{
	// disable interrupts
	if(isEmpty(S.q))
	{
		S.count++;
		//enable interrupts
		return;
	}
	ResumeThread(dequeue(S.q));
}

int main()
{
	return 0;
}