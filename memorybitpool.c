/**
Memory bit pool
*/

unsigned int NUM = 0;
unsigned int SIZE = 0;
unsigned int* bitmap = 0;
void* ptr = 0;

const unsigned int BITS_PER_BYTE = 8;
const unsigned int UINT_BIT_SIZE = sizeof(unsigned int) * BITS_PER_BYTE;

void initialize(unsigned int __NUM, unsigned int __SIZE)
{
	SIZE = __SIZE;
	NUM = __NUM/UINT_BITS_SIZE;
	ptr = malloc(__SIZE*__NUM);
	
	bitmap = (unsigned int*) malloc(NUM*sizeof(unsigned int));
}

void * allocate_slot()
{
	if(!ptr) return NULL;
	unsigned int i = 0;
	for(i = 0; i<NUM; i++)
	{
		if(bitmap[i] != 0xFFFFFFFF) break;
	}
	if(i == NUM) return NULL;
	else
	{
		unsigned int temp = bitmap[i];
		unsigned int cnt = 0;
		while(temp & 0x01 != 0)
		{
			cnt++;
			temp >>= 1;
		}
		
		temp = 1;
		bitmap[i] = bitmap[i] | (temp << cnt);
		
		unsigned int slot_number = i*UINT_BITS_SIZE + cnt;
		
		return (void *) ((unsigned char*) ptr + slot_number*SIZE);
	}
}

void free_slot (void * p)
{
	if(!p) return;
	else
	{
		unsigned int slot_number = (p - ptr)/SIZE;
		unsigned int i = slot_number/UINT_BITS_SIZE;
		unsigned int cnt = slot_number%UINT_BITS_SIZE;
		unsigned int temp = 1;
		temp <<= cnt;
		bitmap[i] = bitmap[i] & (~temp);
	}
}