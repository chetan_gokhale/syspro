#include <stdlib.h>

void* _aligned_malloc(unsigned int size, unsigned int alignment)
{
	unsigned int disp;
	unsigned char* ptr_alloc = malloc(size+alignment+sizeof(unsigned int));
	if(!ptr_alloc) return ptr_alloc;
	ptr_alloc = ptr_alloc + sizeof(unsigned int);
	disp = alignment - ((unsigned int)ptr_alloc%alignment);
	ptr_alloc += disp;
	*((unsigned int*)ptr_alloc - 1) = disp;
	
	return ptr_alloc;
}

void _aligned_free(void* _ptr_alloc)
{
	unsigned char* ptr_alloc = (unsigned char*)_ptr_alloc;
	unsigned int disp = *((unsigned int *)ptr_alloc - 1);
	ptr_alloc = ptr_alloc - disp - sizeof(unsigned int);
	free(ptr_alloc);
}