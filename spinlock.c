#include <Windows.h>

getCurrentTID()
{
	return 0;
}

typedef struct __spinlock_t
{
	volatile unsigned int lock;
	volatile unsigned int tid;
} spinlock_t;

void spin_lock(spinlock_t *lock)
{
	for(;;)
	{
		/*preempt_disable();*/
		if(InterlockedExchange(&lock->lock, 1) == 0)
		{
			lock->tid = getCurrentTID();
			break;
		}
		/*preempt_enable();*/
		while(lock->lock == 1);
	}
}

void spin_unlock(spinlock_t *lock)
{
	if(getCurrentTID() == lock->tid)
	{
		lock->lock = 0;
		/*preempt_enable();*/
	}
}

int main()
{
	return 0;
}