#include <Windows.h>

#define __numThreads 5

void __syncthreads()
{
	static volatile unsigned int __epochCount = 0;
	static volatile unsigned int __threadCount = 0;

	unsigned int thisEpoch = __epochCount;
	unsigned int thisThread = InterlockedIncrement(&__threadCount);
	if (thisThread == __numThreads)
	{
		__epochCount++;
		__threadCount = 0;
	}
	while(thisEpoch  == __epochCount);
}

DWORD WINAPI MyThreadFunction(LPVOID lpParam)
{
	static int count = 0;
	count++;
	__syncthreads();
	printf("%d\n", count);
	__syncthreads();
	printf("****\n");
	count++;
	__syncthreads();
	printf("%d\n", count);
}

int main()
{
	HANDLE  hThreadArray[__numThreads];
	LPVOID lpParam = 0;
	DWORD   dwThreadIdArray[__numThreads];
	
	int i = 0;
	for(i = 0; i < __numThreads; i++)
	{
        hThreadArray[i] = CreateThread( 
            NULL,                   // default security attributes
            0,                      // use default stack size  
            MyThreadFunction,       // thread function name
            lpParam,	            // argument to thread function 
            0,                    	// use default creation flags
			&dwThreadIdArray[i]);
			
		if(hThreadArray[i] == NULL)
		{
			printf("Error Occurred While Creating Threads\n");
			return 3;
		}
	}
	
    WaitForMultipleObjects(__numThreads, hThreadArray, TRUE, INFINITE);

    // Close all thread handles and free memory allocations.

    for(i=0; i<__numThreads; i++)
    {
        CloseHandle(hThreadArray[i]);
    }
	
	return 0;
}