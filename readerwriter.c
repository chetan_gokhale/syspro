/**
Pseudo code for reader-writer synchronization problem
*/

Mutex write_mutex, read_mutex;
volatile unsigned int readcount = 0;

void write()
{
	m.acquire();
	// write data
	m.release();
}

void read()
{
	read_mutex.acquire();
	readcount++;
	if(readcount == 1)
		write_mutex.acquire();
	read_mutex.release();
	
	// read data
	
	read_mutex.acquire();
	readcount--;
	if(!readcount)
		write_mutex.release();
	read_mutex.release();
}