/**
Pseudo Code for Producer Consumer (Bounded Buffer) Problem
*/
#define N	5
Semaphore Full(0);
Semaphore Empty(N);
Mutex m;

void producer()
{
	Empty.wait();
	m.acquire();
	// Produce one data item
	m.release();
	Full.signal();
}

void consumer()
{
	Full.wait();
	m.acquire();
	// Consume one data item
	m.release();
	Empty.signal();
}